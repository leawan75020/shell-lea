1 Vous devez entrer “man mkdir”

 Man mkdir entre User Commands

2 devez créer 3 répertoires “rep-toto”, “rep-titi”, “rep-tata” en imbriqué.

 $ mkdir rep-toto rep-titi rep-tata
 $ ls
 all-new  rep-tata  rep-titi  rep-toto  test4
3 vous devez créer 3 fichiers “toto”, “titi”, “tata” dans chacun des répertoires que vous venez de créer.
 $ cd rep-tata

 ~/rep-tata ⌚ 15:56:32
 $ touch toto.txt tata.txt titi.txt

 ~/rep-tata ⌚ 15:57:04
 $ ls
 tata.txt  titi.txt  toto.txt

 ~/rep-tata ⌚ 15:57:06
 $ cd ../

 ~ ⌚ 15:57:22
 $ cd rep-toto

 ~/rep-toto ⌚ 15:57:36
 $ touch toto.txt tata.txt titi.txt

 ~/rep-toto ⌚ 15:58:04
 $ ls
 tata.txt  titi.txt  toto.txt

 ~/rep-toto ⌚ 15:58:06
 $ cd ../

 ~ ⌚ 15:58:13
 $ cd rep-titi

 ~/rep-titi ⌚ 15:58:24
 $ touch toto.txt tata.txt titi.txt

 ~/rep-titi ⌚ 15:58:43
 $ ls
 tata.txt  titi.txt  toto.txt
4 vous devez créer un symlink portant le nom de 42 pour accéder au répertoire rep-toto que vous venez de créer.

 ln -s [fichier cible] [Nom de fichier symbolique]

 $ ln -s rep-toto 42
5 Vous devez lister les informations des répertoires avec une commande vous permettant de voir les fichiers cachés et permettre une lecture humaine du poid de chaque fichier.

 $ ls -al  or $ ls -a -l
6 vous devez installer “oh-my-zsh”.

 sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
7 vous devez installer le template “robbyrussell” avec “oh-my-zsh”.

 $ vim .zshrc ensuite changer le zsh- theme  “ robbyrussell ”. 
 Escape + :wq
8 vous devez installer vim.

 $ sudo apt install vim or lea@LAPTOP-6F2913BJ:~$ sudo apt-get install vi
9 vous devez définir “vim” comme éditeur par défaut.

 root@LAPTOP-6F2913BJ:/home/lea# update-alternatives --config editor 
 root@LAPTOP-6F2913BJ:/home/lea# update-alternatives --config editor
 There are 4 choices for the alternative editor (providing /usr/bin/editor).

   Selection    Path                Priority   Status
   ------------------------------------------------------------
     0            /bin/nano            40        auto mode
       1            /bin/ed             -100       manual mode
         2            /bin/nano            40        manual mode
	 * 3            /usr/bin/vim.basic   30        manual mode
	   4            /usr/bin/vim.tiny    15        manual mode

	   Press <enter> to keep the current choice[*], or type selection number: 3
	   root@LAPTOP-6F2913BJ:/home/lea# root@LAPTOP-6F2913BJ:/
	   bash: root@LAPTOP-6F2913BJ:/: No such file or directory
	   root@LAPTOP-6F2913BJ:/home/lea# exit
	   exit
10 vous devez installer “git”.

 $ sudo apt install git
11 vous devez configurer les short cuts “ci”, “co” ,“br” et “st” pour GIT.

 lea@LAPTOP-6F2913BJ:~$ git config --global alias.st status
 lea@LAPTOP-6F2913BJ:~$ git config --global alias.br branch
 lea@LAPTOP-6F2913BJ:~$ git config --global alias.ci commit
 lea@LAPTOP-6F2913BJ:~$ git config --global alias.co checkout

 lea@LAPTOP-6F2913BJ:~$ cat .gitconfig
 [alias]
         st = status
	         br = branch
		         ci = commit
			         co = checkout
12 vous devez copier tous les répertoires dans “rep-toto” vers un nouveau répertoire 42-cpy.

 $ cp -r rep-toto 42-cpy
13 vous devez renomer le dossier “42-cpy” vers “42-cpy-rename”.

 $ mv 42-cpy 42-cpy-rename

14 vous devez copier “42-cpy-rename” vers “42-cpy-rename-v2” puis supprimer le repertoire “42-cpy-rename”.

 $ cp -r 42-cpy-rename 42-cpy-rename-v2

 $ rm -rf 42-cpy-rename
15.	 Vous devez faire en sorte que votre répertoire “42-cpy-rename-v2” est la sortie : drwxr-xr-x

$ chmod 751 42-cpy-rename-v2

16 vous devez créer une nouvel utilisateur bot42.

 sudo adduser bot42  

17.	Vous devez donner les droits root à bot42.

root@LAPTOP-6F2913BJ:/home/lea# useradd bot42 -g root

18 vous  devez créer une répertoire “root-42” avec le super user.
 ➜  ~ sudo su
 root@LAPTOP-6F2913BJ:/home/lea# mkdir root-42

19 vous devez supprimer le répertoire “root-42” avec l’utilisateur bot42.

 bot42@LAPTOP-6F2913BJ:/home/lea$ rm -rf root-42

20 vous devez obtenir le résulat suivant sur le fichier “42-letter-count”: 42 42-letter-count

 -rw-r--r-- 1 bot42 admin    0 Apr  2 15:47 42
 -rw-r--r-- 1 bot42 admin    0 Apr  2 15:47 42-letter-count
21 vous devez créer le fichier “42-user” avec le super user puis changez les droits utilisateurs de root vers bot42.

root@LAPTOP-6F2913BJ:/home/lea# chown bot42 42-user
22 vous devez créer un fichier “my-file-42” et utiliser un “ls” avec la commande “cut” pour ne faire apparaitre que les permissions “-rw-r--r--”

 lea@LAPTOP-6F2913BJ:~$ ls -l |cut -c11-
 or
 lea@LAPTOP-6F2913BJ:~$ ls -l |cut -b11-

 1 lea   lea    0 Apr  2 23:41 my-file-42

23 vous devez créer une tâche récurente qui va créer une ligne de log “i love 42” toutes les minutes dans un fichier “my-log-42.log”

 lea@LAPTOP-6F2913BJ:~$ cat my-log-42.log
 #!/bin/sh
 # */1 * * * * /home/linux/sh/crontab

 echo "i love 42">> /home/my-log-42.log
 i love 42

24 vous devez afficher l’intégralité d’un fichier de log et voir en temps réel l’ajout de nouelles lignes sur le fichier “my-log-42.log” dans votre terminal.

 lea@LAPTOP-6F2913BJ:~$ tail -f my-log-42.log

25 créez le fichier “toto-42” et éditer le pour lui faire prendre un poid de “42 ko”.

lea@LAPTOP-6F2913BJ:~$ cat toto-42
#!/bin/bash
echo "42 Ko"
26 vous devez changer les permissions du fichier “toto-42” pour lui donner les droits suivants : “rw---x-wx”

 lea@LAPTOP-6F2913BJ:~$ chmod 613 toto-42
 lea@LAPTOP-6F2913BJ:~$ ls -al
 -rw---x-wx 1 lea   lea     26 Apr  5 18:18 toto-42
27 ouvrez “Google Chrome” et trouvez le moyen de fermer “google chrome”.


 lea@LAPTOP-6F2913BJ:~$ vim ~/.zshrc
 # alias chrome='open -a 'Google Chrome''
 :wq

 lea@LAPTOP-6F2913BJ:~$ cd /mnt
 lea@LAPTOP-6F2913BJ:/mnt$ ls
 c
 lea@LAPTOP-6F2913BJ:/mnt$ cd c
 lea@LAPTOP-6F2913BJ:/mnt/c$ chrome
28 vous classer en temps réel la prise en charge CPU de tout les processus de votre sytème.

 lea@LAPTOP-6F2913BJ:~$ top
29 vous devez compter le nombre de ligne dans le fichier “/etc/passwd”

 lea@LAPTOP-6F2913BJ:~$ cat /etc/passwd
 total : 41 lignes

30 vous devez télécharger la page “https://www.google.fr” dan un fichier index.html

 lea@LAPTOP-6F2913BJ:~$ sudo touch index.html
 lea@LAPTOP-6F2913BJ:~$ chmod a+x index.html
 lea@LAPTOP-6F2913BJ:~$ wget -P /home/lea/index.html
 /home/lea/index.html:Scheme missing.

 
